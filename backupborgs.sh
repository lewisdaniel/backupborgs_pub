#!/bin/bash
#
# Pretty simple backupscript for borgbackup
# 
# Daniel Lewis
# https://bitbucket.org/lewisdaniel/backupborgs_pub
# 
# borgbackup: https://github.com/borgbackup/borg
# rclone: https://github.com/ncw/rclone
#

# read the config file
source config

### functions

# sanity checkes
sanity_dirs() {
    if [ -d $TARGET ]; then
        printf "   All repositories will be created in $TARGET\n\n"
    else
        printf "\n   $TARGET doesnt exist \n"
        exit
    fi

    for i in "${array[@]}"; do
        if [ ! -d "$i" ]; then
            printf "   Directory "$i" doesnt exist \n"
            exit
        else
            printf "   Ready to backup: "$i"\n"
        fi
    done
}

# borg
borg_init() {
    for i in "${array2[@]}"; do
        printf "\n\n    Initiating borg repo: "$i"\n"
        borg init --encryption=keyfile $TARGET/"$i"
    done
}

borg_create() {
    for index in ${!array[*]}; do
        printf "\n\n    Creating new archiv in repo: "$index"\n"
        borg create $CREATE_OPTS $TARGET/"${array2[$index]}"::$DATE "${array[$index]}"
    done
}

borg_prune() {
    if [ $PRUNE -eq 1 ];
    then
        for i in "${array2[@]}"; do
            printf "\n\n    Cleaning old archives: "$i"\n"
            borg prune $PRUNE_OPTS $TARGET/"$i"
        done
    else
        printf "\n\n    Prune function is off \n"
    fi
}

## execcute functions
sanity_dirs
borg_init
borg_create
borg_prune

# calling backupborgs_upload.sh
/bin/bash backupborgs_upload.sh
