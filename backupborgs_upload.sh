#!/bin/bash
#
# read the config file
source config

# remote to backblaze b2
rclone_b2() {
    if [ $USE_B2 -eq 1 ];
    then
        for i in "${array2[@]}"; do
            printf "\n\n    Uploading "$i" to B2 Bucket "$i"\n"
            rclone copy $RCLONE_OPTS $TARGET/"$i" $B2:"$i"
        done
    else
        printf "\n\n    Not using B2"
    fi
}

# remote to google drive
rclone_gd() {
    if [ $USE_GDRIVE -eq 1 ];
    then
        for i in "${array2[@]}"; do
            printf "\n\n    Uploading "$i" to Google Drive "$GDPATH/$i"\n"
            rclone copy $RCLONE_OPTS $TARGET/"$i" $GDRIVE:$GDPATH/"$i"
        done
    else
        printf "\n\n    Not using GDrive"
    fi
}

## execute functions
rclone_b2
rclone_gd
